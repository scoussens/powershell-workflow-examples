﻿workflow Cireson-NewADUser
{
    Param
    (
        [Parameter(Mandatory=$True)]
        [string]$Username,

        [Parameter(Mandatory=$False)]
        [string]$DisplayName,
        
        [Parameter(Mandatory=$False)]
        [string]$Description = "None provided.",

        [Parameter(Mandatory=$False)]
        [string]$Password = "Cireson123",

        [Parameter(Mandatory=$False)]
        [string]$FirstName,

        [Parameter(Mandatory=$False)]
        [string]$LastName,

        [Parameter(Mandatory=$False)]
        [string]$City,

        [Parameter(Mandatory=$False)]
        [string]$Country,

        [Parameter(Mandatory=$False)]
        [string]$PostalCode,

        [Parameter(Mandatory=$False)]
        [string]$State,

        [Parameter(Mandatory=$False)]
        [string]$StreetAddress,

        [Parameter(Mandatory=$False)]
        [string]$Company,

        [Parameter(Mandatory=$False)]
        [string]$Department,

        [Parameter(Mandatory=$False)]
        [string]$EmployeeID,

        [Parameter(Mandatory=$False)]
        [string]$EmployeeNumber,

        [Parameter(Mandatory=$False)]
        [string]$Manager,

        [Parameter(Mandatory=$False)]
        [string]$Office,

        [Parameter(Mandatory=$False)]
        [string]$Title,

        [Parameter(Mandatory=$False)]
        [string]$Fax,

        [Parameter(Mandatory=$False)]
        [string]$HomePhone,

        [Parameter(Mandatory=$False)]
        [string]$MobilePhone,

        [Parameter(Mandatory=$False)]
        [string]$OfficePhone,

        [Parameter(Mandatory=$False)]
        [string]$EmailAddress,

        [Parameter(Mandatory=$False)]
        [string]$ExpirationDate,

        [Parameter(Mandatory=$False)]
        [bool]$ChangePassword = $False,

        [Parameter(Mandatory=$False)]
        [bool]$PasswordNeverExpires = $True
    )

    #Uses the activity provided in the Active Directory integration module of the box in SMA
    New-ADUser `
        -Name $Username `
        -DisplayName $DisplayName `
        -UserPrincipalName ($Username + "@lab.dev") `
        -Description $Description `
        -Enable $True `
        -AccountPassword (ConvertTo-SecureString -AsPlainText $Password -Force) `
        -GivenName $FirstName `
        -Surname $LastName `
        -City $City `
        -Country $Country `
        -PostalCode $PostalCode `
        -State $State `
        -StreetAddress $StreetAddress `
        -Company $Company `
        -Department $Department `
        -EmployeeID $EmployeeID `
        -EmployeeNumber $EmployeeNumber `
        -Manager $Manager `
        -Office $Office `
        -Title $Title `
        -Fax $Fax `
        -HomePhone $HomePhone `
        -MobilePhone $MobilePhone `
        -OfficePhone $OfficePhone `
        -EmailAddress $EmailAddress `
        -AccountExpirationDate $ExpirationDate `
        -ChangePasswordAtLogon $ChangePassword `
        -PasswordNeverExpires $PasswordNeverExpires `
        -Path "OU=Test Accounts,OU=Lab Users,DC=lab,DC=dev"
}