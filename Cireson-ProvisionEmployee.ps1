﻿workflow Cireson-ProvisionEmployee
{
    Param
    (
        [parameter(Mandatory=$True)]
        [string]$Firstname,
        
        [parameter(Mandatory=$True)]
        [string]$Lastname,
        
        [parameter(Mandatory=$True)]
        [string]$City,
        
        [parameter(Mandatory=$False)]
        [int]$VacationDays = 10,
        
        [parameter(Mandatory=$False)]
        [int]$Compensation = 0,
        
        [parameter(Mandatory=$False)]
        [DateTime]$StartDate
    )

    $SCSMServerName = "SCSM01.lab.dev"
    $ADConnector = "LabADConnector"
    $irTitle = "New Employee Workflow Failed $(Get-Date)"
    $irClassification = "Other Problems"
    $irSupportGroup = "Tier 1"
    
    try {
        #Create the Employee Display Name for database input and AD creation.
        $EmployeeName = $Firstname + " " + $Lastname

        if($StartDate -eq "Monday, January 1, 0001 12:00:00 AM") {$StartDate = Get-Date}
    
        #Update the HR DB.  Uses the Invoke-Sqlcmd activity that is provided out of the box in SMA, but only if you've installed the SQL powershell modules'.
        $ID = [guid]::NewGuid()
        $Result = Invoke-Sqlcmd `
            -Query "USE HRDB; INSERT INTO Employees VALUES ('$ID','$EmployeeName',$Compensation,$VacationDays,'$StartDate')" -HostName SCSQL01.lab.dev -ServerInstance "scsql01.lab.dev"
        
        #Checkpoint after completion of the SQL write
        Checkpoint-Workflow
        
        #Create the user in AD.  Calls a custom SMA Runbook called NewADUser 
        Cireson-NewADUser `
            -UserName $EmployeeName.Replace(" ","") `
            -DisplayName $EmployeeName `
            -Description "Created on $(Get-Date)" `
            -FirstName $Firstname `
            -Lastname $Lastname `
            -City $City
            
        #Checkpoint after completion of the new AD user creation    
        Checkpoint-Workflow
        
        Cireson-RefreshConnector -ConnectorName $ADConnector -ServerName $scsmServerName       

        #Write a success event to the event log.  This calls a separate runbook which uses the out of the box Write-EventLog activity.
        Cireson-WriteEventLog `
            -EventLog "Application" `
            -Message "Successfully provisioned new user $(Get-Date)!" `
            -Source "Windows Error Reporting"
    }
    catch {
        $errorMessage = $_
        
        #Create an incident if something fails.  Calls a custom SMA Runbook called CreateIncident
        Cireson-CreateSCSMIncident `
            -Title $irTitle `
            -Description $errorMessage `
            -Classification $irClassification `
            -SupportGroup $irSupportGroup
        
        Throw $errorMessage       
    }
}
