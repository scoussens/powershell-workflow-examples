﻿workflow Cireson-CreateSCSMIncident
{
    Param(
        [Parameter(Mandatory=$True)]
        [string]$Title,
        
        [Parameter(Mandatory=$True)]
        [string]$Classification,
        
        [Parameter(Mandatory=$False)]
        [string]$Description = "No description provided.",
        
        [Parameter(Mandatory=$False)]
        [string]$SupportGroup,

        [Parameter(Mandatory=$False)]
        [string]$Server = "SCSM01.lab.dev"
    )
    
    #This activity is created from the SMLets cmdlet New-SCSMIncident
    New-SCSMIncident `
    -Description $Description `
    -Classification $Classification `
    -SupportGroup $SupportGroup `
    -Impact Medium `
    -Title $Title `
    -Urgency Medium `
    -Status Active `
    -SCSMSession $Server 
}
