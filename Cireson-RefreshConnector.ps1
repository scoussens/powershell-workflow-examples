﻿workflow Cireson-RefreshConnector
{
    param(
        [parameter(Mandatory=$True)]
        [string]$ConnectorName,
        
        [Parameter(Mandatory=$True)]
        [string]$ServerName
    )
    
    InlineScript {
        Try {
            $ADConnector = Get-SCSMConnector -DisplayName $Using:ConnectorName -ComputerName $Using:ServerName
            Start-SCSMConnector -Connector $ADConnector
        }
        Catch {
            $errorMessage = $_
            Throw $errorMessage
        }
    }
}