﻿workflow Cireson-WriteEventLog
{
    Param
    (
        [Parameter(Mandatory=$True)]
        [string]$Message,
        
        [Parameter(Mandatory=$True)]
        [string]$EventLog,
        
        [Parameter(Mandatory=$True)]
        [string]$Source
    )
    Write-EventLog  -LogName $EventLog -Source $Source -EventID 0 -Message $Message
}
