workflow Cireson-MonitorUpdatedObject {
	param (
		[parameter(Mandatory=$false)]
		[string]$GUID,
		
		[parameter(Mandatory=$false)]
		[string]$Class = 'System.WorkItem.Incident$',
		
		[parameter(Mandatory=$false)]
		[string]$Criteria,
		
		[parameter(Mandatory=$false)]
		[string]$Filter,

        [parameter(Mandatory=$false)]
        [string]$ServerName = "SCSM01.lab.dev"
	)
    
	#Sets the current datetime for comparison to modified date to find changed items
    $LastRunTime = InlineScript {
        if(!(Test-Path c:\WorkflowFiles\MonitorData.xml)) {
            $XmlData = New-Object PSObject
            Add-Member -InputObject $XmlData -MemberType NoteProperty -Name "LastRunTime" -value $(Get-Date -Format s)
            Add-Member -InputObject $XmlData -MemberType NoteProperty -Name "StartTime" -value $(Get-Date -Format s)
            $XmlData | Export-Clixml c:\WorkflowFiles\MonitorData.xml
            $LastRunTime = Get-Date -Format s
        } else {
	        $XmlData = Import-Clixml c:\WorkflowFiles\MonitorData.xml
            $LastRunTime = $XmlData.LastRunTime
            $XmlData.LastRunTime = Get-Date -Format s
            $XmlData | Export-Clixml c:\WorkflowFiles\MonitorData.xml
	    }
        $LastRunTime
    }
    
    $ModifiedIds = InlineScript {
        $Id = Get-SCSMObject `
            -Class (Get-SCSMClass -Name $Using:Class -SCSMSession $Using:ServerName) `
            -SCSMSession $Using:ServerName | `
            Where {$_.LastModified -gt $Using:LastRunTime}
        if($Id.Count -gt 0){
            $Id.Get_ID()
        }
    }

    Return $ModifiedIds.Guid
}
	